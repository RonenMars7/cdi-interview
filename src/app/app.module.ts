import { BrowserModule } from '@angular/platform-browser';
import { NgModule , NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { ViewComponent } from './components/mainView/view.component';
import { TableHeaderComponent } from './components/table-header/table-header.component';
import { TableRowComponent } from './components/table-row/table-row.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    TableHeaderComponent,
    TableRowComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
