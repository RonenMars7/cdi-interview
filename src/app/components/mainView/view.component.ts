import { Component, OnInit } from '@angular/core';
import { TableHeaderComponent } from './../table-header/table-header.component';
import { TableRowComponent } from './../table-row/table-row.component';
import * as configData from './../../../assets/data/config.js';
import * as movieData from './../../../assets/data/data.js';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  configs:any = [];
  movies: any = [];

  constructor() { };

  ngOnInit() {
    this.configs = configData.default;
    this.movies = movieData.default;
  }
}
